package com.example.map226tomandjerrygui;

import domain.validator.FriendshipValidator;
import domain.validator.MessageValidator;
import domain.validator.UserValidator;
import repository.database.EventDbRepo;
import repository.database.FriendshipDbRepo;
import repository.database.MessageDbRepo;
import repository.database.UserDbRepo;
import service.Service;

public class Main {

    public static void main(String[] args) {
        /*
        String userFileName="D:\\Users\\black\\Desktop\\Desktop\\Facultate notite\\Metode avansate de programare\\" +
                "3 Laboratoare\\11.02.2021\\Lab6-7\\userRepo.txt";
        String friendshipFileName="D:\\Users\\black\\Desktop\\Desktop\\Facultate notite\\Metode avansate de programare\\" +
                "3 Laboratoare\\11.02.2021\\Lab6-7\\friendshipRepo.txt";

        UserRepoFile repoUser = new UserRepoFile(userFileName, new UserValidator());
        FriendshipRepoFile repoFriendship = new FriendshipRepoFile(friendshipFileName, new FriendshipValidator(), repoUser);
        * */

        UserDbRepo repoUser = new UserDbRepo("jdbc:postgresql://localhost:5432/labmap",
                "postgres" ,
                "postgres" ,
                new UserValidator());
        FriendshipDbRepo repoFriendship = new FriendshipDbRepo("jdbc:postgresql://localhost:5432/labmap",
                "postgres" ,
                "postgres" ,
                new FriendshipValidator());
        MessageDbRepo repoMessage = new MessageDbRepo("jdbc:postgresql://localhost:5432/labmap",
                "postgres" ,
                "postgres" ,
                new MessageValidator());

        EventDbRepo repoEvent = new EventDbRepo("jdbc:postgresql://localhost:5432/labmap",
                "postgres" ,
                "postgres" );

        Service service = new Service(repoUser, repoFriendship, repoMessage, repoEvent);
    }
}
