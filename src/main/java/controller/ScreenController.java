package controller;

import com.example.map226tomandjerrygui.MainApplication;
import domain.EventMeet;
import domain.User;
import domain.validator.FriendshipValidator;
import domain.validator.MessageValidator;
import domain.validator.UserValidator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import repository.database.EventDbRepo;
import repository.database.FriendshipDbRepo;
import repository.database.MessageDbRepo;
import repository.database.UserDbRepo;
import service.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

public class ScreenController {


    protected static User currentUser;

    public static Service service;

    public static Stage stage;

    public static void init() throws IOException {

        UserDbRepo repoUser = new UserDbRepo("jdbc:postgresql://localhost:5432/labmap",
                "postgres",
                "postgres",
                new UserValidator());
        FriendshipDbRepo repoFriendship = new FriendshipDbRepo("jdbc:postgresql://localhost:5432/labmap",
                "postgres",
                "postgres",
                new FriendshipValidator());
        MessageDbRepo repoMessage = new MessageDbRepo("jdbc:postgresql://localhost:5432/labmap",
                "postgres",
                "postgres",
                new MessageValidator());

        EventDbRepo repoEvent = new EventDbRepo("jdbc:postgresql://localhost:5432/labmap",
                "postgres" ,
                "postgres" );

        service = new Service(repoUser, repoFriendship, repoMessage, repoEvent);
    }

    @FXML
    protected void changeScene(String fxmlString) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource(fxmlString));

        Scene scene = new Scene(fxmlLoader.load());
        scene.setFill(Color.TRANSPARENT);

        stage.setScene(scene);
    }

    @FXML
    protected void loginScreenSwitch(boolean userAdded) throws IOException{
        stage.hide();
        changeScene("login.fxml");
        stage.show();
        if (userAdded)
            giveConfirmation("User added successfully!");
    }

    @FXML
    protected void registerScreenSwitch() throws IOException{
        stage.hide();
        changeScene("signUp.fxml");
        stage.show();
    }

    @FXML
    protected void adminScreenSwitch() throws IOException {
        changeScene("admin.fxml");
    }

    @FXML
    protected void homeScreenSwitch() throws IOException {
        changeScene("home.fxml");
    }

    @FXML
    protected void friendsScreenSwitch() throws IOException {
        changeScene("friends.fxml");
    }

    @FXML
    protected void messagesScreenSwitch() throws IOException {
        changeScene("messages.fxml");
    }

    @FXML
    protected void eventsScreenSwitch() throws IOException {
        changeScene("events.fxml");
    }

    @FXML
    protected void statisticsScreenSwitch() throws IOException {
        changeScene("statistics.fxml");
    }

    @FXML
    protected void closeWindow() throws IOException {
        System.exit(0);
    }

    @FXML
    protected void giveConfirmation(String s) {
        Alert a = new Alert(Alert.AlertType.CONFIRMATION);
        a.setHeaderText(null);
        a.setContentText(s);
        a.show();
    }

    @FXML
    protected void giveWarning(String s) {
        Alert a = new Alert(Alert.AlertType.ERROR);
        a.setHeaderText(null);
        a.setContentText(s);
        a.show();
    }
}
