package controller;

import domain.*;
import domain.validator.FriendshipValidator;
import domain.validator.MessageValidator;
import domain.validator.UserValidator;
import javafx.scene.control.Alert;
import repository.database.EventDbRepo;
import repository.database.FriendshipDbRepo;
import repository.database.MessageDbRepo;
import repository.database.UserDbRepo;
import service.Service;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class AdminController {

    @FXML
    TextField text_search;

    @FXML
    TableView<User> table1;

    @FXML
    TableColumn<User, Long> t1_id;
    @FXML
    TableColumn<User, String> t1_fname;
    @FXML
    TableColumn<User, String> t1_lname;


    @FXML
    TableView<FriendshipDTO> table2;
    @FXML
    TableColumn<FriendshipDTO, String> t2_status;
    @FXML
    TableColumn<FriendshipDTO, LocalDateTime> t2_date;
    @FXML
    TableColumn<FriendshipDTO, Long> t2_to;
    @FXML
    TableColumn<FriendshipDTO, Long> t2_from;

    @FXML
    TableView<User> table3;
    @FXML
    TableColumn<User, Long> t3_id;
    @FXML
    TableColumn<User, String> t3_fname;
    @FXML
    TableColumn<User, String> t3_lname;

    @FXML
    TableView<Message> table4;
    @FXML
    TableColumn<Message, String> t4_message;
    @FXML
    TextField text_message;

    private Service service;

    ObservableList<User> friends = FXCollections.observableArrayList();
    ObservableList<User> allUsers = FXCollections.observableArrayList();
    ObservableList<FriendshipDTO> friendships = FXCollections.observableArrayList();
    ObservableList<Message> messages = FXCollections.observableArrayList();

    /*
        public void setService(Service service) {
        this.service = service;
    }
     */



    @FXML
    public void initialize(){

        UserDbRepo repoUser = new UserDbRepo("jdbc:postgresql://localhost:5432/labmap",
                "postgres" ,
                "postgres" ,
                new UserValidator());
        FriendshipDbRepo repoFriendship = new FriendshipDbRepo("jdbc:postgresql://localhost:5432/labmap",
                "postgres" ,
                "postgres" ,
                new FriendshipValidator());
        MessageDbRepo repoMessage = new MessageDbRepo("jdbc:postgresql://localhost:5432/labmap",
                "postgres" ,
                "postgres" ,
                new MessageValidator());

        EventDbRepo repoEvent = new EventDbRepo("jdbc:postgresql://localhost:5432/labmap",
                "postgres" ,
                "postgres" );

        this.service = new Service(repoUser, repoFriendship, repoMessage, repoEvent);


        t1_id.setCellValueFactory(
                new PropertyValueFactory<User, Long>("id")
        );
        t1_fname.setCellValueFactory(
                new PropertyValueFactory<User,String>("firstName")
        );
        t1_lname.setCellValueFactory(
                new PropertyValueFactory<User,String>("lastName")
        );

        t2_to.setCellValueFactory(
                new PropertyValueFactory<FriendshipDTO, Long>("leftId")
        );

        t2_from.setCellValueFactory(
                new PropertyValueFactory<FriendshipDTO, Long>("rightId")
        );

        t2_date.setCellValueFactory(
                new PropertyValueFactory<FriendshipDTO, LocalDateTime>("date")
        );

        t2_status.setCellValueFactory(
                new PropertyValueFactory<FriendshipDTO, String>("status")
        );

        t3_id.setCellValueFactory(
                new PropertyValueFactory<User, Long>("id")
        );
        t3_fname.setCellValueFactory(
                new PropertyValueFactory<User,String>("firstName")
        );
        t3_lname.setCellValueFactory(
                new PropertyValueFactory<User,String>("lastName")
        );

        t4_message.setCellValueFactory(
                new PropertyValueFactory<Message, String>("message")
        );

        List<User> allUsersList = StreamSupport.stream(service.getUsers().spliterator(), false).collect(Collectors.toList());
        allUsers = FXCollections.observableArrayList(allUsersList);
        table3.setItems(allUsers);

    }

    Long searchedUserID = 0L;
    User searchedUser = null;

    private void refreshFriends(){
        List<FriendshipDTO> fr = service.getFriendshipsForUsers(searchedUserID);

        friendships.clear();
        friendships = FXCollections.observableArrayList(fr);
        table2.setItems(friendships);
    }

    private void refreshMessages(long u1, long u2){
        List<Message> mes = service.getConversation(u1, u2, true, false);
        //messages.removeIf(x->{x.getTo().size()})
        messages.clear();
        messages = FXCollections.observableArrayList(mes);
        table4.setItems(messages);

    }

    @FXML
    protected void btn_search_click () {

        try {
            searchedUserID = Long.parseLong(text_search.getText());
            searchedUser = service.findUser(searchedUserID);
            if (searchedUser == null) {
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setContentText("This ID doesn't belong to any user !");
                a.show();
                return;
            } else {
                messages.clear();
                selectedUser = null;
                // table4.setItems(messages);

                Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                a.setContentText("Welcome back " + searchedUser.getLastName() + " " + searchedUser.getFirstName());
                a.show();
            }

            /*

             */
            List<Tuple<User, LocalDateTime>> usersDate = service.findFriendships(searchedUserID);

            friends.clear();
            usersDate.forEach(x -> {
                friends.add(x.getLeft());
            });

            table1.setItems(friends);

            refreshFriends();

        }
        catch (NumberFormatException nfe){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Invalid ID !");
            a.show();
        }

    }

    @FXML
    protected void btn_add_click (){
        if(searchedUserID.equals(0L)){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Login into a user !");
            a.show();
            return;
        }
        User userToAdd = table3.getSelectionModel().getSelectedItem();
        if(userToAdd != null){
            try {
                if (service.sendFriendRequest(searchedUserID, userToAdd.getId(), LocalDateTime.now()) != null){
                    service.removeFriendship(searchedUserID, userToAdd.getId());
                }
                service.sendFriendRequest(searchedUserID, userToAdd.getId(), LocalDateTime.now());
                refreshFriends();
                //friends.add(userToAdd);
            }
            catch (Exception e){
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setContentText(e.getMessage());
                a.show();
            }
        }
    }

    @FXML
    protected void btn_remove_click (){
        User userToRemove = table1.getSelectionModel().getSelectedItem();
        if(searchedUserID.equals(0L)){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Login into a user !");
            a.show();
            return;
        }
        if(userToRemove != null) {
            service.removeFriendship(searchedUserID, userToRemove.getId());
            refreshFriends();
            friends.removeIf(x -> x.getId().equals(userToRemove.getId()));
        }
        else{
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Select the user you want to remove !");
            a.show();
        }
    }

    @FXML
    protected void btn_cancelFriendRequest_click(){
        FriendshipDTO friendshipToRemove = table2.getSelectionModel().getSelectedItem();
        if(searchedUserID.equals(0L)){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Login into a user !");
            a.show();
            return;
        }
        if(friendshipToRemove != null) {
            if(!friendshipToRemove.getStatus().equals("pending")){
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setContentText("Friendship must be pending !");
                a.show();
                return;
            }
            if(!friendshipToRemove.getLeftId().equals(searchedUserID)){
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setContentText("This friend request was receive, you have to accept/reject it!");
                a.show();
                return;
            }
            service.removeFriendship(searchedUserID, friendshipToRemove.getRightId());
            refreshFriends();
        }
        else{
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Select the friend request you want to cancel !");
            a.show();
        }
    }

    @FXML
    protected void btn_acceptFriendRequest_click(){
        FriendshipDTO friendshipSelected = table2.getSelectionModel().getSelectedItem();
        if(searchedUserID.equals(0L)){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Login into a user !");
            a.show();
            return;
        }
        if(friendshipSelected != null) {
            if(!friendshipSelected.getStatus().equals("pending")){
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setContentText("Friendship must be pending !");
                a.show();
                return;
            }
            if(!friendshipSelected.getRightId().equals(searchedUserID)){
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setContentText("This friend request was sent, you can only cancel it!");
                a.show();
                return;
            }
            service.respondFriendship(searchedUserID, friendshipSelected.getLeftId(),"approved");
            refreshFriends();
            User userToAdd = service.findUser(friendshipSelected.getRightId());
            friends.add(userToAdd);
        }
        else{
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Select the friend request you want to accept !");
            a.show();
        }
    }

    @FXML
    protected void btn_denyFriendRequest_click(){
        FriendshipDTO friendshipToRemove = table2.getSelectionModel().getSelectedItem();
        if(searchedUserID.equals(0L)){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Login into a user !");
            a.show();
            return;
        }
        if(friendshipToRemove != null) {
            if(!friendshipToRemove.getStatus().equals("pending")){
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setContentText("Friendship must be pending !");
                a.show();
                return;
            }
            if(!friendshipToRemove.getRightId().equals(searchedUserID)){
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setContentText("This friend request was sent, you can only cancel it!");
                a.show();
                return;
            }
            service.respondFriendship(searchedUserID, friendshipToRemove.getLeftId(),"rejected");
            refreshFriends();
        }
        else{
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Select the friend request you want to deny !");
            a.show();
        }
    }

    User selectedUser;

    @FXML
    protected void t1_get_row(){
        selectedUser = table1.getSelectionModel().getSelectedItem();
        if (selectedUser != null){
            refreshMessages(searchedUserID ,selectedUser.getId());
        }
    }


    @FXML
    protected void btn_send_click (){
        if (selectedUser != null && searchedUser != null){
            String message = text_message.getText();
            if (Objects.equals(message.strip(), ""))
                return;
            Long idReply = 0L;
            Message replyMessage = table4.getSelectionModel().getSelectedItem();
            if (replyMessage != null)
                idReply = replyMessage.getId();
            service.addMessage(searchedUserID, List.of(selectedUser.getId()), message, idReply);
            refreshMessages(searchedUserID, selectedUser.getId());
            text_message.clear();
        }
    }
}