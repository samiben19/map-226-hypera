# Hypera - Messenger app

*Made by: [Sarkozi Samuel](https://github.com/samiben19) and [Pruteanu Robert](https://github.com/Iovva)*

---
## Pictures

![Login](photos/login.png)
![Register](photos/register.png)
![Home](photos/home.gif)
![Friends](photos/friends.png)
![Messages](photos/messages.png)
![Events](photos/events.png)
![Statistics](photos/statistics.png)

---